I have documented the endpoint usage on the endpoints with an example for each in src.main.java.com.plawler.account.AccountController. I made some assumptions which should also be documented in the endpoints, such as unique userName, non-unique Names and Emails. The tests that test the endpoints can be found in AccountApplicationTests. The tests specific to meeting the requirements specified in the tech challenge document are:

postAccountSuccess()
deleteAccountSuccess()
getAccountByUsernameSuccess()
updateUserNameSuccess()
updateUserEmailSuccess()
updateUserEmailAndNameSuccess()