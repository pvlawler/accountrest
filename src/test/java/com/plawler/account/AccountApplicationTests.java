package com.plawler.account;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.plawler.account.domain.Account;
import com.plawler.account.domain.PostAccountParams;
import com.plawler.account.domain.UpdateAccountParams;

@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
public class AccountApplicationTests {
	
	private MockMvc mockMvc;
	private ObjectMapper objectMapper = new ObjectMapper();
	private String email = "email";
	private String name = "name";
	private String userName = "userName";
	private String atGmail = "@gmail.com";
	
	private String accountPath = "/account";
	
	@Autowired
	private WebApplicationContext webApplicationContext;

	@Before
	public void setup() throws Exception {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}
	
	@After
	public void teardown() throws Exception {
		this.mockMvc = null;
	}

	
	@Test
	public void nonExistentEndpoint() throws Exception {
		mockMvc.perform(get("/some/path"))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(status().isNotFound());
	}
	
	@Test
	public void requestWithInvalidContentType() throws Exception {
		int addToFields = 77;
		PostAccountParams generatedAccount = new PostAccountParams(userName + addToFields, name + addToFields, email + addToFields + atGmail);
		
		mockMvc.perform(post(accountPath)
				.content(jsonify(generatedAccount))
				.contentType(MediaType.APPLICATION_PDF_VALUE))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(status().isUnsupportedMediaType())
                .andExpect(status().reason(containsString("Content type 'application/pdf' not supported")));
	}
	
	@Test
	public void requestWithNoContentType() throws Exception {
		int addToFields = 4911212;
		PostAccountParams generatedAccount = new PostAccountParams(userName + addToFields, name + addToFields, email + addToFields + atGmail);
		
		mockMvc.perform(post(accountPath)
				.content(jsonify(generatedAccount)))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(status().isUnsupportedMediaType())
                .andExpect(status().reason(containsString("Content type 'null' not supported")));
	}
	
	@Test
	public void accountNotFound() throws Exception {
		int invalidAccountId = 999;
		
		mockMvc.perform(get(accountPath + "/" + invalidAccountId))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(status().isNotFound());
	}
	
	@Test
	public void postAccountNullFields() throws Exception {
		int addToFields = 11;
		
		PostAccountParams noUserNameAccount = new PostAccountParams(null, name + addToFields, email + addToFields + atGmail);
		
		mockMvc.perform(post(accountPath)
				.content(jsonify(noUserNameAccount))
				.contentType(MediaType.APPLICATION_JSON_VALUE))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(status().isBadRequest());
		
		PostAccountParams noNameAccount = new PostAccountParams(userName + addToFields, null, email + addToFields + atGmail);
		
		mockMvc.perform(post(accountPath)
				.content(jsonify(noNameAccount))
				.contentType(MediaType.APPLICATION_JSON_VALUE))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(status().isBadRequest());
		
		PostAccountParams noEmailAccount = new PostAccountParams(userName + addToFields, name + addToFields, null);
		
		mockMvc.perform(post(accountPath)
				.content(jsonify(noEmailAccount))
				.contentType(MediaType.APPLICATION_JSON_VALUE))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(status().isBadRequest());
	}
	
	@Test
	public void postAccountInvalidEmail() throws Exception {
		int addToFields = 11;
		
		PostAccountParams noUserNameAccount = new PostAccountParams(userName + addToFields, name + addToFields, email + "butNoAtGmail");
		
		mockMvc.perform(post(accountPath)
				.content(jsonify(noUserNameAccount))
				.contentType(MediaType.APPLICATION_JSON_VALUE))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(status().isBadRequest());
	}
	
	@Test
	public void postAccountSuccess() throws Exception {
		int addToFields = 33;
		PostAccountParams fakeAccount = new PostAccountParams(userName + addToFields, name + addToFields, email + addToFields + atGmail);
		
		MvcResult result = mockMvc.perform(post(accountPath)
				.content(jsonify(fakeAccount))
				.contentType(MediaType.APPLICATION_JSON_VALUE))
		        .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk())
				.andReturn();
		
		Integer accountId = deserialize(result.getResponse().getContentAsString(), Integer.class);
		assertNotNull(accountId);
		assertTrue(accountId > 0);
	}
	
	@Test
	public void postTwoAccountsIncrementIds() throws Exception {
		int addToFieldsOne = 7728;
		PostAccountParams accountOne = new PostAccountParams(userName + addToFieldsOne, name + addToFieldsOne, email + addToFieldsOne + atGmail);
		
		MvcResult result = mockMvc.perform(post(accountPath)
				.content(jsonify(accountOne))
				.contentType(MediaType.APPLICATION_JSON_VALUE))
		        .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk())
				.andReturn();
		
		Integer accountIdOne = deserialize(result.getResponse().getContentAsString(), Integer.class);
		assertNotNull(accountIdOne);
		assertTrue(accountIdOne > 0);
		
		int addToFieldsTwo = 9928;
		PostAccountParams accountTwo = new PostAccountParams(userName + addToFieldsTwo, name + addToFieldsTwo, email + addToFieldsTwo + atGmail);
		
		MvcResult resultTwo = mockMvc.perform(post(accountPath)
				.content(jsonify(accountTwo))
				.contentType(MediaType.APPLICATION_JSON_VALUE))
		        .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk())
				.andReturn();
		
		Integer accountIdTwo = deserialize(resultTwo.getResponse().getContentAsString(), Integer.class);
		assertNotNull(accountIdTwo);
		assertTrue(accountIdTwo > 0);
		
		assertEquals(accountIdOne + 1, accountIdTwo.intValue());
	}
	
	@Test
	public void getAccountByUsernameSuccess() throws Exception {
		int addToFields = 1234;
		String accountUserName = userName + addToFields;
		PostAccountParams postAccountParams = new PostAccountParams(accountUserName, name + addToFields, email + addToFields + atGmail);
		
		mockMvc.perform(post(accountPath)
				.content(jsonify(postAccountParams))
				.contentType(MediaType.APPLICATION_JSON_VALUE))
		        .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk());
		
		MvcResult result = mockMvc.perform(get(accountPath)
				.param("searchText", accountUserName)
				.param("searchFields","userName"))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk())
				.andReturn();
		
		List<Account> accounts = getList(result.getResponse().getContentAsString(), Account.class);
		assertEquals(1, accounts.size());
		assertEquals(accounts.get(0).getUserName(), accountUserName);	
	}
	
	@Test
	public void getAccountByIdSuccess() throws Exception {
		int addToFields = 2234;
		String accountUserName = userName + addToFields;
		PostAccountParams postAccountParams = new PostAccountParams(accountUserName, name + addToFields, email + addToFields + atGmail);
		
		MvcResult result = mockMvc.perform(post(accountPath)
				.content(jsonify(postAccountParams))
				.contentType(MediaType.APPLICATION_JSON_VALUE))
		        .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk())
				.andReturn();
		
		Integer accountId = deserialize(result.getResponse().getContentAsString(), Integer.class);
		assertNotNull(accountId);
		assertTrue(accountId > 0);
		
		MvcResult accountResult = mockMvc.perform(get(accountPath + "/" + accountId))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk())
				.andReturn();
		
		Account account = deserialize(accountResult.getResponse().getContentAsString(), Account.class);
		assertNotNull(account);
		assertEquals(accountId.intValue(), account.getAccountId());
	}
	
	@Test
	public void deleteAccountNotFound() throws Exception {
		int nonExistentAccountId = 999;
		
		mockMvc.perform(delete(accountPath + "/" + nonExistentAccountId))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(status().isNotFound());
	}
	
	@Test
	public void deleteAccountBadId() throws Exception {
		int badAccountId = 0;
		
		mockMvc.perform(delete(accountPath + "/" + badAccountId))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(status().isBadRequest());
	}
	
	@Test
	public void deleteAccountSuccess() throws Exception {
		int addToFields = 23834;
		String accountUserName = userName + addToFields;
		PostAccountParams fakeAccount = new PostAccountParams(accountUserName, name + addToFields, email + addToFields + atGmail);
		
		mockMvc.perform(post(accountPath)
				.content(jsonify(fakeAccount))
				.contentType(MediaType.APPLICATION_JSON_VALUE))
		        .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk());
		
		MvcResult result = mockMvc.perform(get(accountPath)
				.param("searchText", accountUserName)
				.param("searchFields","userName"))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andReturn();
		
		List<Account> accounts = getList(result.getResponse().getContentAsString(), Account.class);
		assertEquals(1, accounts.size());
		assertEquals(accounts.get(0).getUserName(), accountUserName);
		
		int accountId = accounts.get(0).getAccountId();
		
		mockMvc.perform(delete(accountPath + "/" + accountId))
			.andDo(MockMvcResultHandlers.print())
			.andExpect(status().isOk());
		
		mockMvc.perform(get(accountPath + "/" + accountId))
		.andDo(MockMvcResultHandlers.print())
		.andExpect(status().isNotFound());
	}
	
	@Test
	public void updateAccountNotFound() throws Exception {
		int idToAdd = 1234;
		int invalidAccountId = 11223344;
		UpdateAccountParams badUpdateAccountParams = new UpdateAccountParams(name + idToAdd, email + idToAdd + atGmail);
		
		mockMvc.perform(put(accountPath + "/" + invalidAccountId)
				.content(jsonify(badUpdateAccountParams))
				.contentType(MediaType.APPLICATION_JSON_VALUE))
		        .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isNotFound());
	}
	
	@Test
	public void updateAccountNullFields() throws Exception {
		int addToFields = 662234;
		String accountUserName = userName + addToFields;
		PostAccountParams postAccountParams = new PostAccountParams(accountUserName, name + addToFields, email + addToFields + atGmail);
		
		MvcResult result = mockMvc.perform(post(accountPath)
				.content(jsonify(postAccountParams))
				.contentType(MediaType.APPLICATION_JSON_VALUE))
		        .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk())
				.andReturn();
		
		Integer accountId = deserialize(result.getResponse().getContentAsString(), Integer.class);
		assertNotNull(accountId);
		assertTrue(accountId > 0);
		
		UpdateAccountParams badUpdateAccountParams = new UpdateAccountParams();
		mockMvc.perform(put(accountPath + "/" + accountId)
				.content(jsonify(badUpdateAccountParams))
				.contentType(MediaType.APPLICATION_JSON_VALUE))
		        .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isBadRequest());
	}
	
	@Test
	public void updateUserEmailSuccess() throws Exception {
		int addToFields = 1177234;
		String accountUserName = userName + addToFields;
		String originalEmail = "original" + email + addToFields + atGmail;
		String updateEmail = "update" + email + addToFields + atGmail;
		
		PostAccountParams postAccountParams = new PostAccountParams(accountUserName, name + addToFields, originalEmail);
		
		MvcResult result = mockMvc.perform(post(accountPath)
				.content(jsonify(postAccountParams))
				.contentType(MediaType.APPLICATION_JSON_VALUE))
		        .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk())
				.andReturn();
		
		Integer accountId = deserialize(result.getResponse().getContentAsString(), Integer.class);
		assertNotNull(accountId);
		assertTrue(accountId > 0);
		
		UpdateAccountParams updateAccountParams = new UpdateAccountParams(null, updateEmail);
		mockMvc.perform(put(accountPath + "/" + accountId)
				.content(jsonify(updateAccountParams))
				.contentType(MediaType.APPLICATION_JSON_VALUE))
		        .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk());
		
		MvcResult accountResult = mockMvc.perform(get(accountPath + "/" + accountId))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk())
				.andReturn();
		
		Account account = deserialize(accountResult.getResponse().getContentAsString(), Account.class);
		assertNotNull(account);
		assertEquals(account.getAccountId(), accountId.intValue());
		assertEquals(account.getEmail(), updateEmail);
	}
	
	@Test
	public void updateUserNameSuccess() throws Exception {
		int addToFields = 98732;
		String accountUserName = userName + addToFields;
		String originalName = "original" + name + addToFields;
		String updateName = "update" + name + addToFields;
		
		PostAccountParams postAccountParams = new PostAccountParams(accountUserName, originalName, email + addToFields + atGmail);
		
		MvcResult result = mockMvc.perform(post(accountPath)
				.content(jsonify(postAccountParams))
				.contentType(MediaType.APPLICATION_JSON_VALUE))
		        .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk())
				.andReturn();
		
		Integer accountId = deserialize(result.getResponse().getContentAsString(), Integer.class);
		assertNotNull(accountId);
		assertTrue(accountId > 0);
		
		UpdateAccountParams updateAccountParams = new UpdateAccountParams(updateName, null);
		mockMvc.perform(put(accountPath + "/" + accountId)
				.content(jsonify(updateAccountParams))
				.contentType(MediaType.APPLICATION_JSON_VALUE))
		        .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk());
		
		MvcResult accountResult = mockMvc.perform(get(accountPath + "/" + accountId))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk())
				.andReturn();
		
		Account account = deserialize(accountResult.getResponse().getContentAsString(), Account.class);
		assertNotNull(account);
		assertEquals(account.getAccountId(), accountId.intValue());
		assertEquals(account.getName(), updateName);
	}
	
	@Test
	public void updateUserEmailAndNameSuccess() throws Exception {
		int addToFields = 40012;
		String accountUserName = userName + addToFields;
		String originalEmail = "original" + email + addToFields + atGmail;
		String updateEmail = "update" + email + addToFields + atGmail;
		String originalName = "original" + name + addToFields;
		String updateName = "update" + name + addToFields;
		
		PostAccountParams postAccountParams = new PostAccountParams(accountUserName, originalName, originalEmail);
		
		MvcResult result = mockMvc.perform(post(accountPath)
				.content(jsonify(postAccountParams))
				.contentType(MediaType.APPLICATION_JSON_VALUE))
		        .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk())
				.andReturn();
		
		Integer accountId = deserialize(result.getResponse().getContentAsString(), Integer.class);
		assertNotNull(accountId);
		assertTrue(accountId > 0);
		
		UpdateAccountParams updateAccountParams = new UpdateAccountParams(updateName, updateEmail);
		mockMvc.perform(put(accountPath + "/" + accountId)
				.content(jsonify(updateAccountParams))
				.contentType(MediaType.APPLICATION_JSON_VALUE))
		        .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk());
		
		MvcResult accountResult = mockMvc.perform(get(accountPath + "/" + accountId))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk())
				.andReturn();
		
		Account account = deserialize(accountResult.getResponse().getContentAsString(), Account.class);
		assertNotNull(account);
		assertEquals(account.getAccountId(), accountId.intValue());
		assertEquals(account.getName(), updateName);
		assertEquals(account.getEmail(), updateEmail);
	}
	
	@Test
	public void searchByNamesSuccess() throws Exception {
		int addToFieldsOne = 69029;
		int addToFieldsTwo = 3342;
		String name = "Joe";
		
		PostAccountParams postAccountParamsOne = new PostAccountParams(userName + addToFieldsOne, name, email + addToFieldsOne + atGmail);
		PostAccountParams postAccountParamsTwo = new PostAccountParams(userName + addToFieldsTwo, name, email + addToFieldsTwo + atGmail);
		
		mockMvc.perform(post(accountPath)
				.content(jsonify(postAccountParamsOne))
				.contentType(MediaType.APPLICATION_JSON_VALUE))
		        .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk());
		
		mockMvc.perform(post(accountPath)
				.content(jsonify(postAccountParamsTwo))
				.contentType(MediaType.APPLICATION_JSON_VALUE))
		        .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk());
		
		MvcResult result = mockMvc.perform(get(accountPath)
				.param("searchText", name)
				.param("searchFields","name"))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk())
				.andReturn();
		
		List<Account> accounts = getList(result.getResponse().getContentAsString(), Account.class);
		assertEquals(2, accounts.size());
	}
	
	@Test
	public void searchByEmailSuccess() throws Exception {
		int addToFieldsOne = 9090909;
		int addToFieldsTwo = 277665;
		String email = "Joe" + atGmail;
		
		PostAccountParams postAccountParamsOne = new PostAccountParams(userName + addToFieldsOne, name + addToFieldsOne, email);
		PostAccountParams postAccountParamsTwo = new PostAccountParams(userName + addToFieldsTwo, name + addToFieldsTwo, email);
		
		mockMvc.perform(post(accountPath)
				.content(jsonify(postAccountParamsOne))
				.contentType(MediaType.APPLICATION_JSON_VALUE))
		        .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk());
		
		mockMvc.perform(post(accountPath)
				.content(jsonify(postAccountParamsTwo))
				.contentType(MediaType.APPLICATION_JSON_VALUE))
		        .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk());
		
		MvcResult result = mockMvc.perform(get(accountPath)
				.param("searchText", email)
				.param("searchFields","email"))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk())
				.andReturn();
		
		List<Account> accounts = getList(result.getResponse().getContentAsString(), Account.class);
		assertEquals(2, accounts.size());
	}
	
	@Test
	public void searchAcrossFieldsSuccess() throws Exception {
		int addToFieldsOne = 355422;
		int addToFieldsTwo = 56642;
		int addToFieldsThree = 99823;
		String same = "JoeJoe";
		
		PostAccountParams postAccountParamsOne = new PostAccountParams(userName + addToFieldsOne, same, email + addToFieldsOne + atGmail);
		PostAccountParams postAccountParamsTwo = new PostAccountParams(userName + addToFieldsTwo, same, email + addToFieldsTwo + atGmail);
		PostAccountParams postAccountParamsThree = new PostAccountParams(same, same, email + addToFieldsThree + atGmail);
		
		mockMvc.perform(post(accountPath)
				.content(jsonify(postAccountParamsOne))
				.contentType(MediaType.APPLICATION_JSON_VALUE))
		        .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk());
		
		mockMvc.perform(post(accountPath)
				.content(jsonify(postAccountParamsTwo))
				.contentType(MediaType.APPLICATION_JSON_VALUE))
		        .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk());
		
		mockMvc.perform(post(accountPath)
				.content(jsonify(postAccountParamsThree))
				.contentType(MediaType.APPLICATION_JSON_VALUE))
		        .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk());
		
		MvcResult result = mockMvc.perform(get(accountPath)
				.param("searchText", same)
				.param("searchFields","userName,name"))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk())
				.andReturn();
		
		List<Account> accounts = getList(result.getResponse().getContentAsString(), Account.class);
		assertEquals(3, accounts.size());
	}
	
	@Test
	public void searchAccountsNoSearchText() throws Exception {

		mockMvc.perform(get(accountPath)
				.param("searchFields","email"))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(status().isBadRequest());
	}
	
	@Test
	public void searchAccountsInvalidSearchField() throws Exception {
		
		mockMvc.perform(get(accountPath)
				.param("searchText", "Joe")
				.param("searchFields","userName,name,blah"))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(status().isBadRequest());
	}
	
	private String jsonify(Object object) throws Exception {
		return objectMapper.writer().writeValueAsString(object);
	}
	
	private <T> List<T> getList(String jsonString, Class<T> clazz) throws JsonParseException, JsonMappingException, IOException {
		return objectMapper.readValue(jsonString, objectMapper.getTypeFactory().constructCollectionType(List.class, clazz));
	}
	
	private <T> T deserialize(String jsonString, Class<T> clazz) throws JsonParseException, JsonMappingException, IOException {
		return objectMapper.readValue(jsonString, clazz);
	}
}
