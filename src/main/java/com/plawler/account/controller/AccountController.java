package com.plawler.account.controller;

import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.plawler.account.domain.Account;
import com.plawler.account.domain.PostAccountParams;
import com.plawler.account.domain.UpdateAccountParams;
import com.plawler.account.service.AccountService;

@Validated
@RestController
@RequestMapping(value = "/account")
public class AccountController {
	
	@Autowired
	private AccountService accountService;

	/**
	 * Returns accounts that match the <code>searchText<code> in any of the <code>searchFields<code>
	 * 
	 * @param searchText - the text to search for
	 * @param searchFields - a comma-separated list of the fields to search across, can also be a single field. Valid fields: name, userName, email
	 * 
	 * @return a <code>List<code> of matching <code>Account<code>
	 * 
	 * example: GET /account?searchText=Joe&searchFields=name,userName
	 */
	@ResponseStatus(code = HttpStatus.OK)
	@RequestMapping(method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public Set<Account> searchAccount(@RequestParam("searchText") String searchText, @RequestParam("searchFields") String searchFields) {
		return accountService.searchAccount(searchText, searchFields);
	}

	/**
	 * Retrieves the account with the associated accountId
	 * 
	 * @param accountId - the accountId
	 * 
	 * @return a <code>Account<code>
	 * 
	 * example: GET /account/1
	 */
	@ResponseStatus(code = HttpStatus.OK)
	@RequestMapping(value = "/{accountId}", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public Account getAccount(@PathVariable("accountId") int accountId) {
		return accountService.getAccountById(accountId);
	}
	
	/**
	 * Creates an account with the given name, userName, and email posted as a json in the body. 
	 * The userName must be unique, names and emails need not be.
	 * 
	 * @return an <code>Integer<code> accountId for the newly created account
	 * 
	 * example: POST /account with JSON for Body: {"name":"Joe","userName":"JoeCool09","email":"JoeCool@gmail.com"}
	 */
	@ResponseStatus(code = HttpStatus.OK)
	@RequestMapping(method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
	public int postAccount(@RequestBody @Valid PostAccountParams postAccountParams) {
		return accountService.postAccount(postAccountParams);
	}
	
	/**
	 * Updates the account of given accountId, with the values as json in the body. 
	 * The only updateable fields are name and email.
	 * 
	 * @param accountId - the accountId
	 * 
	 * example: PUT /account/1  - with JSON for Body: {"name":"Joe","email":"JoeCool@gmail.com"}
	 */
	@ResponseStatus(code = HttpStatus.OK)
	@RequestMapping(value = "/{accountId}", method=RequestMethod.PUT, produces=MediaType.APPLICATION_JSON_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
	public void putAccount(@PathVariable("accountId") int accountId, @RequestBody @Valid UpdateAccountParams updateAccountParams) {
		accountService.updateAccount(accountId, updateAccountParams);
	}
	
	/**
	 * Deletes the account with the given accountId
	 * 
	 * @param accountId - the accountId
	 * 
	 * example: DELETE /account/1
	 */
	@ResponseStatus(code = HttpStatus.OK)
	@RequestMapping(value = "/{accountId}", method=RequestMethod.DELETE, produces=MediaType.APPLICATION_JSON_VALUE)
	public void deleteAccount(@PathVariable("accountId") int accountId) {
		accountService.deleteAccount(accountId);
	}
}
