package com.plawler.account.service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.plawler.account.dao.AccountDao;
import com.plawler.account.domain.Account;
import com.plawler.account.domain.PostAccountParams;
import com.plawler.account.domain.UpdateAccountParams;
import com.plawler.account.exception.BadRequestException;
import com.plawler.account.exception.NotFoundException;

@Service
public class AccountService {
	
	@Autowired
	private AccountDao accountDao;
	
	private static int nextId = 1;
	private final String USERNAME = "userName";
	private final String NAME = "name";
	private final String EMAIL = "email";
	private final Set<String> VALID_SEARCH_FIELDS = new HashSet<>(Arrays.asList(USERNAME, NAME, EMAIL));
	
	public Set<Account> searchAccount(@NotEmpty String searchText, @NotEmpty String searchFields) {		
		List<String> fields = Arrays.asList(searchFields.split(","));
		fields.forEach(currentField -> {
			if (!VALID_SEARCH_FIELDS.contains(currentField))
				throw new BadRequestException(currentField + " is not a valid searchField");
		});
		
		Set<Account> allAccountMatches = new HashSet<>();
		if (fields.contains(USERNAME)) {
			Optional<Account> userNameAccount = accountDao.getAccountByUserName(searchText);
			userNameAccount.ifPresent(account -> allAccountMatches.add(account));
		}
		
		if (fields.contains(NAME)) {
			Optional<Set<Account>> nameAccounts = accountDao.getAccountsByName(searchText);
			nameAccounts.ifPresent(accounts -> allAccountMatches.addAll(accounts));
		}
		
		if (fields.contains(EMAIL)) {
			Optional<Set<Account>> emailAccounts = accountDao.getAccountsByEmail(searchText);
			emailAccounts.ifPresent(accounts -> allAccountMatches.addAll(accounts));
		}
		
		if (allAccountMatches.isEmpty())
			throw new NotFoundException("no accounts found matching search criteria, searchText " + searchText + " searchFields " + searchFields);
		
		return allAccountMatches;
	}
	
	public Account getAccountById(int accountId) {
		assert accountId > 0;
		
		Optional<Account> account = accountDao.getAccountById(accountId);
		return account.orElseThrow(() -> new NotFoundException("No account with Id " + accountId + " found"));
	}
	
	public int postAccount(PostAccountParams postAccountParams) {
		if (postAccountParams == null)
			throw new BadRequestException("no account data");
		
		if (accountExists(postAccountParams.getUserName()))
			throw new BadRequestException("account with userName already exists");
				
		Account account = new Account(
				nextId++,
				postAccountParams.getUserName(),
				postAccountParams.getName(),
				postAccountParams.getEmail()
		);
		
		accountDao.save(account);
		return account.getAccountId();
	}
	
	public void updateAccount(int accountId, UpdateAccountParams updateAccountParams) {
		if (updateAccountParams == null)
			throw new BadRequestException("no account data");
				
		if (StringUtils.isEmpty(updateAccountParams.getEmail()) && StringUtils.isEmpty(updateAccountParams.getName()))
			throw new BadRequestException("Email, Name or both should be specified when updating");
			
		Account existingAccount = accountDao.getAccountById(accountId)
				.orElseThrow(() -> new NotFoundException("no account for that accountId exists"));
		
		Account newAccount = new Account(
				existingAccount.getAccountId(),
				existingAccount.getUserName(),
				StringUtils.isEmpty(updateAccountParams.getName()) ? existingAccount.getName() : updateAccountParams.getName(),
				StringUtils.isEmpty(updateAccountParams.getEmail()) ? existingAccount.getEmail() : updateAccountParams.getEmail()
		);
		
		accountDao.delete(existingAccount);
		accountDao.save(newAccount);
	}
	
	public void deleteAccount(int accountId) {
		if (accountId < 1)
			throw new BadRequestException("invalid accountId");
		
		Optional<Account> account = accountDao.getAccountById(accountId);
		accountDao.delete(account.orElseThrow(() -> new NotFoundException("no account found")));
	}
	
	private boolean accountExists(String userName) {
		assert !StringUtils.isEmpty(userName);
		
		return accountDao.getAccountByUserName(userName).isPresent();
	}

}
