package com.plawler.account.exception;

public class BadRequestException extends AbstractApplicationException {

	private static final long serialVersionUID = 1L;

	public BadRequestException(String message) {
		super(message);
	}
}
