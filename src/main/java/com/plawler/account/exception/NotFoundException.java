package com.plawler.account.exception;

public class NotFoundException extends AbstractApplicationException {

	private static final long serialVersionUID = 1L;

	public NotFoundException(String message) {
		super(message);
	}
}
