package com.plawler.account.exception;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@Component
@ControllerAdvice
public class ApplicationExceptionHandler {
	
	@ExceptionHandler
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public void handle(NotFoundException exception, HttpServletResponse response) throws IOException {
		response.sendError(HttpStatus.NOT_FOUND.value(), exception.getMessage());
	}

	@ExceptionHandler
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public void handle(BadRequestException exception, HttpServletResponse response) throws IOException {
		response.sendError(HttpStatus.BAD_REQUEST.value(), exception.getMessage());
	}
	
	@ExceptionHandler
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public void handle(ConstraintViolationException e, HttpServletResponse response) throws IOException {
	    StringBuilder strBuilder = new StringBuilder();
	    for (ConstraintViolation<?> violation : e.getConstraintViolations())
	    	strBuilder.append(violation.getMessage() + "\n");
	    
	    response.sendError(HttpStatus.BAD_REQUEST.value(), strBuilder.toString());
	}
	
	@ExceptionHandler
	@ResponseStatus(HttpStatus.UNSUPPORTED_MEDIA_TYPE)
	public void handle(HttpMediaTypeNotSupportedException exception, HttpServletResponse response) throws IOException {
		response.sendError(HttpStatus.UNSUPPORTED_MEDIA_TYPE.value(), exception.getMessage());
	}
}
