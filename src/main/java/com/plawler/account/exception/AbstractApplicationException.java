package com.plawler.account.exception;

public abstract class AbstractApplicationException extends RuntimeException {

	public AbstractApplicationException(String message) {
		super(message);
	}
}
