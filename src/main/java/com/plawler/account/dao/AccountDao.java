package com.plawler.account.dao;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.plawler.account.domain.Account;

@Component
public class AccountDao {

	private Object accountLock = new Object();
	private Map<Integer,Account> accountIdToAccount = new HashMap<>();
	private Map<String,Account> userNameToAccount = new HashMap<>();
	private Set<Account> accounts = new HashSet<>();
	
	public Optional<Account> getAccountById(int accountId) {
		synchronized (accountLock) {
			Account account = accountIdToAccount.get(accountId);
			return account == null ? Optional.empty() : Optional.of(account);
		}
	}
	
	public Optional<Account> getAccountByUserName(String userName) {
		if (StringUtils.isEmpty(userName))
			return Optional.empty();
		
		Account userNameAccount = userNameToAccount.get(userName);
		return userNameAccount == null ? Optional.empty() : Optional.of(userNameAccount);
	}
	
	public void save(Account account) {
		assert account != null;
		
		synchronized (accountLock) {
			accountIdToAccount.put(account.getAccountId(), account);
			userNameToAccount.put(account.getUserName(), account);
			accounts.add(account);
		}
	}
	
	public Optional<Set<Account>> getAccountsByEmail(String email) {
		if (StringUtils.isEmpty(email))
			return Optional.empty();
		
		synchronized (accountLock) {
			Set<Account> accountsFound = accounts.stream().filter(currentAccount -> currentAccount.getEmail().equals(email)).collect(Collectors.toSet());
			return accountsFound.isEmpty() ? Optional.empty() : Optional.of(accountsFound);
		}
	}
	
	public Optional<Set<Account>> getAccountsByName(String name) {
		if (StringUtils.isEmpty(name))
			return Optional.empty();
		
		synchronized (accountLock) {
			Set<Account> accountsFound = accounts.stream().filter(currentAccount -> currentAccount.getName().equals(name)).collect(Collectors.toSet());
			return accountsFound.isEmpty() ? Optional.empty() : Optional.of(accountsFound);
		}
	}
	
	public void delete(Account account) {
		synchronized (accountLock) {
			accountIdToAccount.remove(account.getAccountId());
			userNameToAccount.remove(account.getUserName());
			accounts.remove(account);
		}
	}
}
